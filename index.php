<?php

// change the following paths if necessary
define('LOCAL', ($_SERVER['SERVER_ADDR'] == '::1' or $_SERVER['SERVER_ADDR'] == '127.0.0.1')); // Сайт работает на локальном компьютере или нет

if (LOCAL) {
    $yii='c:\Users\Don\Dropbox\yii1-sourse\yii-1.1.16.bca042\framework\yii.php';
} else {
    $yii='E:\alexey_d\Dropbox\yii1-sourse\yii-1.1.16.bca042\framework\yii.php';
}

$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();
