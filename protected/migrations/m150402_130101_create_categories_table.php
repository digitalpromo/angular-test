<?php

class m150402_130101_create_categories_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('categories', array(
            'id' => 'pk',
            'title' => 'string NOT NULL',
            'url' => 'string NOT NULL',
        ));
	}

	public function down()
	{
		echo "m150402_130101_create_categories_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}