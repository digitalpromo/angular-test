<?php

class m150402_130154_create_category_item_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('category_item', array(
            'id' => 'pk',
            'id_category' => 'int NOT NULL',
            'title' => 'string NOT NULL',
            'content' => 'text',
        ));
	}

	public function down()
	{
		echo "m150402_130154_create_category_item_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}