<?php

class Categories
{
    /**
     * Получить список главных категорий
     */
    public function getRootCategories() {
        $roots = Category::model()->roots()->findAll();
        return $roots;
    }
    
    public function getCategoryList($rootId) {
        $category = Category::model()->findByPk($rootId);
        $descendants = $category->children()->findAll();
        return $descendants;
    }
}

