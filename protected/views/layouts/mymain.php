<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" ng-controller="mainCtrl">

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="description"/>
<meta name="keywords" content="keywords"/> 
<meta name="author" content="author"/> 
<title>Colorvoid Website Template</title>
<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/css/main.css" type="text/css" />
</head>

<body>

<div id="layout_wrapper">
    <div id="layout_edgetop"></div>
    <div id="layout_container">
    	<div id="site_title">
    		<h1 class="left"><a href="#">Website name</a></h1>
    		<h2 class="right">Your slogan or perhaps a short introductional text</h2>
    		<div class="clearer">&nbsp;</div>
    	</div>
    	<div id="top_separator"></div>
    	<div id="navigation">
    		<div id="tabs" >
    			<ul>
                    <li ng-repeat="item in categories">
                        <a href="#/categories/{{item.id}}">
                            <span>{{item.title}}</span>
                        </a>
                    </li>
    				<!--<li class="current_page_item"><a href="#/"><span>Posts page</span></a></li>
    				<li><a href="#/login"><span>Post comments</span></a></li>
                    <li><a href="#"><span>Sample test page</span></a></li>
    				<li><a href="#"><span>Archives</span></a></li>
    				<li><a href="#"><span>Empty page</span></a></li>-->				
    			</ul>
    			<div class="clearer">&nbsp;</div>
    		</div>
    	</div>
    	<div class="spacer h5"></div>
    	<div id="main">
    		<div class="left" id="main_left">
                <!--***-->
                <!--<div class="loader" ng-show="loader"><div class="loader-content">Loading...</div></div>-->
    			<div id="main_left_content">
                    
                    <ng-view></ng-view>
    			</div>
                <!--***-->
    		</div>
    		<div class="right" id="main_right">
    			<div id="sidebar">
    				<!--<div class="box">
    					<div class="box_title">Search</div>
    					<div class="box_body">
    						<form method="get" id="searchform" action="#">
    						<div>
    							<table class="search">
    							<tr>
    								<td><input type="text" value="" name="s" id="s" /></td>
    								<td style="padding-left: 10px"><input type="image" src="img/button_go.gif" /></td>
    							</tr>
    							</table>
    						</div>
    						</form>
    					</div>
    					<div class="box_bottom"></div>
    				</div>-->
    				<!--<div class="box">
    					<div class="box_title">Archives</div>
    					<div class="box_body">
    						<ul>
    							<li><a href="#">December 2007</a> (5)</li>
    							<li><a href="#">June 2007</a> (2)</li>
    							<li><a href="#">May 2007</a> (6)</li>
    							<li><a href="#">March 2007</a> (12)</li>
    							<li><a href="#">February 2007</a> (8)</li>
    							<li><a href="#">May 2007</a> (10)</li>
    						</ul>
    					</div>
    					<div class="box_bottom"></div>
    				</div>-->
                    <div class="box" >
    					<div class="box_title">Categories</div>
    					<div class="box_body">
    						<ul>
                                <li ng-repeat="item in items">
                                    <a href="#/categories/{{item.root}}/{{item.id}}">{{item.title}}</a>
                                </li>
    						</ul>
    					</div>
    					<div class="box_bottom"></div>
    				</div>
    				<!--<div class="box">
    					<div class="box_title">Links</div>
    					<div class="box_body">
    						<ul>
    							<li><a href="http://templates.arcsin.se/category/blogger-templates/">Blogger Templates</a></li>
    							<li><a href="http://templates.arcsin.se/category/joomla-templates/">Joomla Templates</a></li>
    							<li><a href="http://templates.arcsin.se/professional-templates/">Professional Templates</a></li>
    							<li><a href="http://templates.arcsin.se/category/website-templates/">Website Templates</a></li>
    							<li><a href="http://templates.arcsin.se/category/wordpress-themes/">Wordpress Themes</a></li>
    						</ul>
    					</div>
    					<div class="box_bottom"></div>
    				</div>-->
    				<div class="box">
    					<div class="box_title">Textbox</div>
    					<div class="box_body p10">
                            <current-time my-current-time="format"></current-time>
    					</div>
    					<div class="box_bottom"></div>
    				</div>
    			</div>
    		</div>
    		<div class="clearer">&nbsp;</div>
    	</div>
    	<div id="footer">
    		<div class="left">&#169; 2008 Website name</div>
    		<div class="right"><a href="http://templates.arcsin.se/">Website template</a> by <a href="http://arcsin.se/">Arcsin</a></div>
    		<div class="clearer">&nbsp;</div>
    	</div>
    </div>
    <div id="layout_edgebottom"></div>
</div>
<div style="display:none;"><a href="http://free-templates.ru/">free-templates.ru</a></div>
<script src="js/libs/require.js" data-main="js/main.js"></script>
</body>
</html>
