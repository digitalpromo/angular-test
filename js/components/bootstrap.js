require([
    'require', 
    'angular',
    'ngRoute',
    'app'
], function(require, ng){
    'use strict';
    
    require(['domReady!'], function(document){
        ng.bootstrap(document, ['app']);
    });
});


