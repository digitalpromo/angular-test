define([
    'angular',
    'auth',
    'categories'
], function(ng){
    'use strict';

    var module = ng.module('app', [
        // third-party services
        'ngRoute',
        
        // modules
        'auth',
        'categories'
    ]);
    
    module.run(['$rootScope', function($rootScope){
            //$rootScope.format = 'M/d/yy h:mm:ss a';
            $rootScope.format = 'h:mm:ss a';
    }]);

    module.config(['$routeProvider', function($routeProvider){
        $routeProvider
            .when('/', {
                templateUrl: 'js/components/app/template/main.html',
                controller: 'mainCtrl',
            })
            .otherwise('/');
    }]);
    
    return module;
});