define([
    '../module'
], function(m){
    'use strict';
    
    m.controller(
        'mainCtrl',
        [
            '$rootScope',
            '$scope',
            '$routeParams',
            'CategoryService',
            function($rootScope, $scope, $routeParams, CategoryService){
                $scope.loader = true;
                CategoryService.getCategories().then(function(response){
                    $scope.categories = response.data;
                    $scope.$on('$routeChangeStart', function(event, next, current){
                        $scope.loader = true;
                    });
                    $scope.$on('$routeChangeSuccess', function(event, curr, next){
                        CategoryService.getCategoryItem(curr.params.system).then(function(data){
                            $scope.loader = false;
                            $scope.items = data;
                        });
                    });
                });
            }
        ]);
});