define([

  // This modules definition
  './module',
  
  //controllers
  './controllers/mainCtrl',
  
  //directives
  './directives/currentTime'

], function() {
    
});