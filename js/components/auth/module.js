define([
    'angular'
], function(ng){
    'use strict';
    
    var module = ng.module('auth', []);
    
    module.config(['$routeProvider', function($routeProvider){
            $routeProvider.when('/login',{
                templateUrl: 'js/components/auth/template/login.html',
                controller: 'loginCtrl',
            });
    }]);

    return module;
});