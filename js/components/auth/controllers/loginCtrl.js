define([
    '../module'
],
function(m){
    'use strict';
    
    m
    .controller('loginCtrl', ['$scope', function($scope){
        $scope.credentials = {
            email: '',
            password: '',
            check: false
        }
        $scope.submit = function(credentials) {
            console.log(credentials);
        }
    }]);
});