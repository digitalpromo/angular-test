define([
    'angular'
], function(ng){
    'use strict';
    
    var module = ng.module('categories', []);
    
    module.config(['$routeProvider', function($routeProvider){
        $routeProvider
            .when('/categories/:system', {
                templateUrl: 'js/components/categories/template/categories.html?v=5',
                //controller: 'itemCtrl'
            });
    }]);

    return module;
});