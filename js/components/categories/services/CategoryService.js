define([
    '../module'
], function(m){
    'use strict';

    m
    //.module('categories.services')
    .factory('CategoryService', ['$http', '$q', '$routeParams', function($http, $q, $routeParams){

        return {
            getCategories: function() {
                return $http.get('Category/getCategories');
                var d = $q.defer();
                $http.get('Category/getCategories').success(function(data){
                    d.resolve(data);
                });
                return d.promise;
            },
            getCategoryItem: function(id) {
                var d = $q.defer();
                $http.get('Category/getCategoryItem?id=' + id).success(function(data){
                    d.resolve(data);
                });
                return d.promise;
            }
        }
    }]);
});