define([

  // This modules definition
  './module',
  
  // controllers
  './controllers/categoriesCtrl',
  './controllers/itemCtrl',
  
  // services
  './services/CategoryService'

], function() {});