require.config({
    
    urlArgs: "bust=" + (new Date()).getTime(),
    
    baseUrl: 'js/components', // корень приложения
    
    /**
     * указание директорий/пакетов, для множественной загрузки модулей
     * ищет в заданной директории файл module.js
     * @example - (package "app") -> 
     *                              js/components/app/module.js - first
     *                              js/components/app/main.js - second
     */
    packages: [
        'app',
        'auth',
        'categories'
    ],
    
    /**
     * пути для модулей, которые находятся не в baseUrl
     */
    paths: {
        'domReady': '../libs/requirejs-domready/domReady',
        'angular': '../libs/angular/angular.min',
        'ngRoute': '../libs/angular/angular-route'
    },

    /**
     * для поддержки сторонних модулей описанных не через define
     */
    shim: {
        'angular': {
            exports: 'angular'
        },
        'ngRoute': ['angular']
    },

    /**
     * массив зависимостей
     */
    deps: ['bootstrap']
});